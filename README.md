# AWS Lambda 

Takes JSON input and returns greetings based on the JSON input.

Technology used

1. Java
2. Spring Boot

### package the application
mvn clean compile package

In the AWS Lambda :

Handle > com.example.awslambda.AwsLambdaApplication::handleRequest

package com.example.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsLambdaApplication implements RequestHandler<Request, Response> {

    public static void main(String[] args) {
        SpringApplication.run(AwsLambdaApplication.class, args);
    }

    @Override
    public Response handleRequest(Request request, Context context) {
        return new Response(String.format("Hello from %s %s", request.getFirstName(), request.getLastName()));
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Request {
    private String firstName;
    private String lastName;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Response {
    private String greetings;
}